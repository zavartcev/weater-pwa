var gulp=require('gulp'),
    watch=require('gulp-watch'),
    preFixer=require('gulp-autoprefixer'),
    ugLify=require('gulp-uglify'),
    sass=require('gulp-sass'),
    sourceMaps=require('gulp-sourcemaps'),
    rigger=require('gulp-rigger'),
    cssMin=require('gulp-clean-css'),
    rimRaf=require('rimraf'),
    browserSync=require('browser-sync'),
    reload=browserSync.reload,
    babel = require('gulp-babel');
var path={
    build:{
        html:'build',
        jsons:'build',
        js:'build/js',
        libs:'build/js',
        plugins:'build/js',
        sw:'build/',
        css:'build/css',
        userStyles:'build/css',
        img:'build/img',
        userImg:'build/user-img',
        fonts:'build/fonts'
    },
    src:{
        html:'src/*.html',
        js:'src/js/main.js',
        jsons:'src/jsons/*.json',
        libs:'src/js/parts/libs/*.js',
        plugins:'src/js/plugins.js',
        sw:'src/js/parts/sw/*.js',
        style:'src/css/main.sass',
        userStyles:'src/css/extended.css',
        img:'src/img/**/*',
        userImg:'src/user-img/**/*',
        fonts:'src/fonts/**/*'
    },
    watch:{
        html:'src/**/*.html',
        js:'src/js/**/*.js',
        jsons:'src/jsons/*.json',
        libs:'src/js/parts/libs/*.js',
        plugins:'src/js/plugins.js',
        sw:'src/js/parts/sw/*.js',
        style:'src/css/**/*.sass',
        userStyles:'src/css/extended.css',
        img:'src/img/**/*',
        fonts:'src/fonts/**/*'
    },
    clean:'build'
};
gulp.task('html:build',function(){
    "use strict";
    gulp.src(path.src.html)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream:true}));
});
gulp.task('js:build',function(){
    "use strict";
    gulp.src(path.src.js)
        .pipe(babel())
        .pipe(rigger())
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream:true}));
});

gulp.task('plugins:build',function(){
    "use strict";
    gulp.src(path.src.plugins)
        .pipe(babel())
        .pipe(rigger())
        .pipe(sourceMaps.init())
        .pipe(ugLify())
        .pipe(sourceMaps.write('/map'))
        .pipe(gulp.dest(path.build.plugins))
        .pipe(reload({stream:true}));
});

gulp.task('sw:build',function(){
    "use strict";
    gulp.src(path.src.sw)
        .pipe(gulp.dest(path.build.sw))
        .pipe(reload({stream:true}));
});

gulp.task('lib:build',function(){
    "use strict";
    gulp.src(path.src.libs)
        .pipe(gulp.dest(path.build.libs))
        .pipe(reload({stream:true}));
});

gulp.task('style:build',function(){
    "use strict";
    gulp.src(path.src.style)
        .pipe(sass().on('error', sass.logError))
        .pipe(sourceMaps.init())
        .pipe(preFixer())
        .pipe(cssMin())
        .pipe(sourceMaps.write('/map'))
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream:true}));
});

gulp.task('userStyle:build',function(){
    "use strict";
    gulp.src(path.src.userStyles)
        .pipe(gulp.dest(path.build.userStyles))
        .pipe(reload({stream:true}));
});
gulp.task('copyimg',function(){
    "use strict";
    gulp.src(path.src.img)
        .pipe(gulp.dest(path.build.img));
    gulp.src(path.src.userImg)
        .pipe(gulp.dest(path.build.userImg));
});
gulp.task('copyJson',function(){
    "use strict";
    gulp.src(path.src.jsons)
        .pipe(gulp.dest(path.build.jsons));
});
gulp.task('copyfonts',function(){
    "use strict";
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts));
});
gulp.task('watch',function(){
    "use strict";
    watch([path.watch.js],function(ev,callback){
        gulp.start('js:build');
    });
    watch([path.watch.plugins],function(ev,callback){
        gulp.start('plugins:build');
    });
    watch([path.watch.sw],function(ev,callback){
        gulp.start('sw:build');
    });
    watch([path.watch.libs],function(ev,callback){
        gulp.start('lib:build');
    });
    watch([path.watch.html],function(ev,callback){
        gulp.start('html:build');
    });
    watch([path.watch.style],function(ev,callback){
        gulp.start('style:build');
    });
    watch([path.watch.fonts],function(ev,callback){
        gulp.start('copyfonts');
    });
    watch([path.watch.img],function(ev,callback){
        gulp.start('copyimg');
    });
    watch([path.watch.jsons],function(ev,callback){
        gulp.start('copyJson');
    });
});
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: path.build.html
        },
        port: 8000,
        serveStaticOptions: {
            extensions: [ 'js', 'css', 'png', 'jpe?g', 'gif', 'svg', 'eot', 'otf', 'ttc', 'ttf', 'json', 'woff2?' ] // pretty urls
        }
    });
});
gulp.task('clean',function(){
    "use strict";
    rimRaf(path.clean,callback);
});
gulp.task('build',[
    'html:build',
    'js:build',
    'sw:build',
    'lib:build',
    'plugins:build',
    'style:build',
    'userStyle:build',
    'copyfonts',
    'copyimg',
    'copyJson'
]);

gulp.task('default',['build','browser-sync','watch']);
